package com.mercadolibre.mutantes.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.mercadolibre.mutantes.model.Subject;

/**
 * 2021/10/18
 * @author Maikol Arley Cucunuba Salazar
 *
 */
public interface SubjectRepository extends MongoRepository<Subject, String> {

    Long countBySubjectType(String subjectType);
}
