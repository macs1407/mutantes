package com.mercadolibre.mutantes.detector;


import java.util.regex.Pattern;

import com.mercadolibre.mutantes.exception.InputValidationException;

/**
 * 2021/10/18
 * @author Maikol Arley Cucunuba Salazar
 *
 */
public class MutantDetector implements Detector {

    Utils utils = new Utils();

    private char[][] dnaSequence;


    /**
     * Comprueba si un adn es de mutante
     *
     * @param dna
     * @return
     * @throws InputValidationException
     */
    public boolean isMutant(String[] dna) throws InputValidationException {

        dnaSequence = populateDnaSequence(dna);

        if (dnaSequence.length < 4) {
            return false;
        }

        return analyzeDNA();
    }

    /**
     * @retur
     */
    private Boolean analyzeDNA() {
        if (utils.horizontalRead(dnaSequence)) return true;
        if (utils.verticalRead(dnaSequence)) return true;
        if (utils.bottomDiagonalsFromLeftReadWithoutMainDiagonal(dnaSequence)) return true;
        if (utils.bottomDiagonalsFromRightReadWithoutMainDiagonal(dnaSequence)) return true;
        if (utils.topDiagonalsFromLeftReadWithMainDiagonal(dnaSequence)) return true;
        return utils.topDiagonalsFromRightReadWithMainDiagonal(dnaSequence);

    }

    /**
     * Mientras se va llenando el arreglo, comprueba que las secuencias de adn sean correctas
     *
     * @param dna
     * @return
     * @throws InputValidationException
     */
    private char[][] populateDnaSequence(String[] dna) throws InputValidationException {
        int dnaSequenceRange = dna.length;
        Pattern pattern = Pattern.compile("[acgt]+", Pattern.CASE_INSENSITIVE);
        dnaSequence = new char[dnaSequenceRange][dnaSequenceRange];
        for (int range = 0; range < dnaSequenceRange; range++) {
            if (dna[range].length() != dnaSequenceRange) {
                throw new InputValidationException("El largo de la secuencia de ADN debe ser igual al numero de secuencias");
            } else if (!pattern.matcher(dna[range]).matches()) {
                throw new InputValidationException("El ADN es Alienigena. El ADN humano y mutante contiene unicamente los caracteres A, C, G y T.");
            } else {
                dnaSequence[range] = dna[range].toUpperCase().toCharArray();
            }
        }
        return dnaSequence;
    }

}
