package com.mercadolibre.mutantes.detector;

import com.mercadolibre.mutantes.exception.InputValidationException;

/**
 * 2021/10/18
 * @author Maikol Arley Cucunuba Salazar
 *
 */
public interface Detector {

    /**
     * Comprueba si un adn es mutante
     *
     * @param dna
     */
    boolean isMutant(String[] dna) throws InputValidationException;
}
