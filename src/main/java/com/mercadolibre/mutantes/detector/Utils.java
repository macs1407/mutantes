package com.mercadolibre.mutantes.detector;

import com.mercadolibre.mutantes.Configuration;

/**
 * 2021/10/18
 * @author Maikol Arley Cucunuba Salazar
 *
 */
public class Utils {

    private Configuration configuration = new Configuration();
    private char[][] dnaSequence;
    private int sequenceCount = 0;
    private char lastCharacter;

    /**
     * @return
     */
    public Boolean horizontalRead(char[][] dnaSequence) {
        this.dnaSequence = dnaSequence;
        for (int row = 0; row < dnaSequence.length; row++) {
            lastCharacter = dnaSequence[row][0];
            if (readHorizontalOrVertical(ReadDirections.HORIZONTAL, lastCharacter, row)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return
     */
    public Boolean verticalRead(char[][] dnaSequence) {
        this.dnaSequence = dnaSequence;
        for (int col = 0; col < dnaSequence.length; col++) {
            lastCharacter = dnaSequence[0][col];
            if (readHorizontalOrVertical(ReadDirections.VERTICAL, lastCharacter, col)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @return
     */
    public Boolean bottomDiagonalsFromLeftReadWithoutMainDiagonal(char[][] dnaSequence) {
        this.dnaSequence = dnaSequence;
        for (int row = 1; row < dnaSequence.length; row++) {
            lastCharacter = dnaSequence[row][0];
            if (readDiagonals(DiagonalReadDirections.FROM_LEFT, DiagonalReadType.BELOW_MAIN_DIAGONAL, row, 0)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return
     */
    public Boolean bottomDiagonalsFromRightReadWithoutMainDiagonal(char[][] dnaSequence) {
        this.dnaSequence = dnaSequence;
        for (int row = 1; row < dnaSequence.length; row++) {
            lastCharacter = dnaSequence[row][dnaSequence.length - 1];
            if (readDiagonals(DiagonalReadDirections.FROM_RIGHT, DiagonalReadType.BELOW_MAIN_DIAGONAL, row, dnaSequence.length - 1)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return
     */
    public Boolean topDiagonalsFromLeftReadWithMainDiagonal(char[][] dnaSequence) {
        this.dnaSequence = dnaSequence;
        for (int col = 0; col < dnaSequence.length; col++) {
            lastCharacter = dnaSequence[0][col];
            if (readDiagonals(DiagonalReadDirections.FROM_LEFT, DiagonalReadType.ABOVE_MAIN_DIAGONAL_INCLUDING, 0, col)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return
     */
    public Boolean topDiagonalsFromRightReadWithMainDiagonal(char[][] dnaSequence) {
        this.dnaSequence = dnaSequence;
        for (int col = 1; col < dnaSequence.length; col++) {
            lastCharacter = dnaSequence[0][dnaSequence.length - col];
            if (readDiagonals(DiagonalReadDirections.FROM_RIGHT, DiagonalReadType.ABOVE_MAIN_DIAGONAL_INCLUDING, 0, dnaSequence.length - col)) {
                return true;
            }
        }
        return false;
    }

    private boolean readHorizontalOrVertical(ReadDirections direction, char initialCharacter, int index) {
        int sameCharactersCount = 1;
        char lastCharacter = initialCharacter;
        char currentCharacter;
        for (int subindex = 1;
             dnaSequence.length - subindex + sameCharactersCount >= configuration.getSequencesLength() && subindex < dnaSequence.length;subindex++) {
            currentCharacter = (ReadDirections.HORIZONTAL.equals(direction) ? dnaSequence[index][subindex] : dnaSequence[subindex][index]);
            if (lastCharacter == currentCharacter) {
                sameCharactersCount++;
                if (sameCharactersCount == configuration.getSequencesLength()) {
                    sequenceCount++;
                    sameCharactersCount = 0;
                    if (sequenceCount == configuration.getMinSequencesToBeMutant()) {
                        return true;
                    }
                }
            } else {
                lastCharacter = currentCharacter;
                sameCharactersCount = 1;
            }
        }
        return false;
    }

    private boolean readDiagonals(DiagonalReadDirections leftOrRight, DiagonalReadType aboveOrBelow, int baseN, int baseM) {
        int offset = 1;

        int sameCharactersCount = 1;

        char lastCharacter = dnaSequence[baseN][baseM];
        char currentCharacter;
        boolean bottomReadCondition = baseN + offset < dnaSequence.length;
        boolean topReadCondition = (leftOrRight.equals(DiagonalReadDirections.FROM_LEFT) && baseM + offset < dnaSequence.length ||
                leftOrRight.equals(DiagonalReadDirections.FROM_RIGHT) && baseM - offset >= 0);
        while ((aboveOrBelow.equals(DiagonalReadType.ABOVE_MAIN_DIAGONAL_INCLUDING) && topReadCondition) ||
                (aboveOrBelow.equals(DiagonalReadType.BELOW_MAIN_DIAGONAL) && bottomReadCondition)) {

            currentCharacter = (leftOrRight.equals(DiagonalReadDirections.FROM_LEFT)) ? dnaSequence[baseN + offset][baseM + offset] :
                    dnaSequence[baseN + offset][baseM - offset];
            if (lastCharacter == currentCharacter) {
                sameCharactersCount++;
                if (sameCharactersCount == configuration.getSequencesLength()) {
                    sequenceCount++;
                    sameCharactersCount = 0;
                    if (sequenceCount >= configuration.getMinSequencesToBeMutant()) {
                        return true;
                    }
                }
            } else {
                lastCharacter = currentCharacter;
                sameCharactersCount = 1;
            }

            offset++;
            bottomReadCondition = baseN + offset < dnaSequence.length;

            topReadCondition = (leftOrRight.equals(DiagonalReadDirections.FROM_LEFT) && baseM + offset < dnaSequence.length ||leftOrRight.equals(DiagonalReadDirections.FROM_RIGHT) && baseM - offset >= 0);
        }
        return false;
    }

    public enum ReadDirections {
        HORIZONTAL, VERTICAL
    }

    public enum DiagonalReadDirections {
        FROM_RIGHT, FROM_LEFT
    }

    public enum DiagonalReadType {
        BELOW_MAIN_DIAGONAL, ABOVE_MAIN_DIAGONAL_INCLUDING
    }

}
