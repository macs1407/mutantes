package com.mercadolibre.mutantes.model;

/**
 * 2021/10/18
 * @author Maikol Arley Cucunuba Salazar
 *
 */
public class DNASequence {

    private String[] dna;

    public String[] getDna() {
        return dna;
    }

    public void setDna(String[] dna) {
        this.dna = dna;
    }

}
