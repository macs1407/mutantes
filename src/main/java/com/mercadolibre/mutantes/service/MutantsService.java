package com.mercadolibre.mutantes.service;

import com.mercadolibre.mutantes.exception.InputValidationException;
import com.mercadolibre.mutantes.exception.ServiceException;

/**
 * 2021/10/18
 * @author Maikol Arley Cucunuba Salazar
 *
 */
public interface MutantsService {

    boolean isMutant(String[] dna) throws ServiceException, InputValidationException;

}
