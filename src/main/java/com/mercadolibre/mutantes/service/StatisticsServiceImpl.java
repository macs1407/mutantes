package com.mercadolibre.mutantes.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercadolibre.mutantes.dao.DAOService;
import com.mercadolibre.mutantes.exception.DbException;
import com.mercadolibre.mutantes.exception.ServiceException;
import com.mercadolibre.mutantes.model.Statistics;

/**
 * 2021/10/18
 * @author Maikol Arley Cucunuba Salazar
 *
 */
@Service
public class StatisticsServiceImpl implements StatisticsService {

    @Autowired
    private DAOService daoService;

    @Override
    public Statistics getStats() throws ServiceException {
        Statistics stats;
        try {
            stats = new Statistics(daoService.getMutantsCount(), daoService.getHumansCount());
        } catch (DbException e) {
            throw new ServiceException(e.getMessage());
        }
        return stats;
    }
}
