package com.mercadolibre.mutantes.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercadolibre.mutantes.dao.DAOService;
import com.mercadolibre.mutantes.detector.Detector;
import com.mercadolibre.mutantes.detector.MutantDetector;
import com.mercadolibre.mutantes.exception.DbException;
import com.mercadolibre.mutantes.exception.InputValidationException;
import com.mercadolibre.mutantes.exception.ServiceException;
import com.mercadolibre.mutantes.model.Subject;

/**
 * 2021/10/18
 * @author Maikol Arley Cucunuba Salazar
 *
 */
@Service
public class MutantsServiceImpl implements MutantsService {

    @Autowired
    private DAOService daoService;

    @Override
    public boolean isMutant(String[] dna) throws InputValidationException, ServiceException {

        Detector mutantDetector = new MutantDetector();
        boolean isMutant;
        final String MUTANT = "Mutant";
        final String HUMAN = "Human";

        try {
            isMutant = mutantDetector.isMutant(dna);
            if (isMutant) {
                Subject mutant = new Subject(dna, MUTANT);
                daoService.insert(mutant);
            } else {
                Subject human = new Subject(dna, HUMAN);
                daoService.insert(human);
            }
        } catch (DbException e) {
            throw new ServiceException(e.getMessage());
        }
        return isMutant;
    }

}
