package com.mercadolibre.mutantes.service;

import com.mercadolibre.mutantes.exception.ServiceException;
import com.mercadolibre.mutantes.model.Statistics;

/**
 * 2021/10/18
 * @author Maikol Arley Cucunuba Salazar
 *
 */
public interface StatisticsService {
	
    Statistics getStats() throws ServiceException;
    
}

