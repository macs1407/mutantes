package com.mercadolibre.mutantes.dao;

import com.mercadolibre.mutantes.exception.DbException;
import com.mercadolibre.mutantes.model.Subject;

/**
 * 2021/10/18
 * @author Maikol Arley Cucunuba Salazar
 *
 */
public interface DAOService {

    /**
     * Metodo que persiste en la base de datos
     *
     * @param subject
     * @throws DbException
     */
    void insert(Subject subject) throws DbException;

    /**
     * Metodo que retorna la cantidad de humanos
     *
     * @return
     * @throws DbException
     */
    int getHumansCount() throws DbException;

    /**
     * Metodo que retorna la cantidad de mutantes
     *
     * @return
     * @throws DbException
     */
    int getMutantsCount() throws DbException;

}
