package com.mercadolibre.mutantes.dao;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercadolibre.mutantes.model.Subject;
import com.mercadolibre.mutantes.repository.SubjectRepository;

/**
 * 2021/10/18
 * @author Maikol Arley Cucunuba Salazar
 *
 */
@Service
public class DAOServiceImpl implements DAOService {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DAOServiceImpl.class);

    @Autowired
    private SubjectRepository subjectRepository;

    public DAOServiceImpl() {
        super();
    }

    /**
     * Metodo que guarda al sujeto en la base de datos
     *
     * @param
     */
    @Override
    public void insert(Subject subject) {
        subjectRepository.save(subject);
        logger.info("Se guardo en la base de datos:" + subject);
    }

    /**
     * Metodo que retorna la cantidad de humanos
     *
     * @return
     */
    @Override
    public int getHumansCount() {

        return subjectRepository.countBySubjectType("Human").intValue();

    }

    /**
     * Metodo que retorna la cantidad de mutantes
     *
     * @return
     */
    @Override
    public int getMutantsCount() {

        return subjectRepository.countBySubjectType("Mutant").intValue();


    }

}

