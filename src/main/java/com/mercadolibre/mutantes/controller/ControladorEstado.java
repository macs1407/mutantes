package com.mercadolibre.mutantes.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mercadolibre.mutantes.exception.ServiceException;
import com.mercadolibre.mutantes.model.Statistics;
import com.mercadolibre.mutantes.service.StatisticsService;

/**
 * 2021/10/18
 * @author Maikol Arley Cucunuba Salazar
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1/stats")
public class ControladorEstado {
	
	private static final Logger logger = LoggerFactory.getLogger(ControladorEstado.class);
	private final String PRODUCES = "application/json; charset=UTF-8";
	
	@Autowired
    private StatisticsService statisticsService;
	
	 /**
     * Metodo que retorna las estadisticas
     *
     * @return a json document que retorna las estadisticas
     */
    @RequestMapping(method = RequestMethod.GET, produces = PRODUCES)
    public Statistics getStatics() {
        Statistics stats = null;
        try {
            stats = statisticsService.getStats();
        } catch (ServiceException e) {
            logger.error(e.getMessage());
        }
        return stats;
    }
}
