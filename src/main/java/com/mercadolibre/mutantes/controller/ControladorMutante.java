package com.mercadolibre.mutantes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mercadolibre.mutantes.exception.InputValidationException;
import com.mercadolibre.mutantes.exception.ServiceException;
import com.mercadolibre.mutantes.model.DNASequence;
import com.mercadolibre.mutantes.service.MutantsService;

/**
 * 2021/10/18
 * @author Maikol Arley Cucunuba Salazar
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1/mutant")
public class ControladorMutante {
	
    @Autowired
    private MutantsService mutantsService;

    /**
     * Metodo que analiza si el adn es de un humano o un mutante
     *
     * @param dna secuencia para analizar
     * @return si el es mutante retorna 200 (OK) si es un humano retorna 403(Forbidden)
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> analyzeMutantCandidateSubject(@RequestBody DNASequence dna) {
        ResponseEntity<String> responseEntity;
        boolean isMutant;
        try {
            isMutant = mutantsService.isMutant(dna.getDna());
            if (isMutant) {
                responseEntity = new ResponseEntity<>(HttpStatus.OK);
            } else {
                responseEntity = new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        } catch (ServiceException e) {
            responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (InputValidationException e) {
            responseEntity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch(Exception e) {
        	if(e != null && e.getMessage().contains("duplicate key")) {
        		responseEntity = new ResponseEntity<>("La secuancia ya existe en base de datos",HttpStatus.INTERNAL_SERVER_ERROR);
        	} else {
        		responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        	}
        }

        return responseEntity;
    }
}
