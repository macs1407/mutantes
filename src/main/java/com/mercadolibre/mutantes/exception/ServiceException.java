package com.mercadolibre.mutantes.exception;

/**
 * 2021/10/18
 * @author Maikol Arley Cucunuba Salazar
 *
 */
public class ServiceException extends Exception {

    private String message;

    public ServiceException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
