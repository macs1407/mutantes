package com.mercadolibre.mutantes;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Assert;

import com.mercadolibre.mutantes.controller.ControladorMutante;
import com.mercadolibre.mutantes.exception.InputValidationException;
import com.mercadolibre.mutantes.exception.ServiceException;
import com.mercadolibre.mutantes.service.MutantsServiceImpl;

/**
 * 2021/10/18
 * 
 * @author Maikol Arley Cucunuba Salazar
 *
 */
public class ControladorMutantesTest {
	private static final String CONTENT_TYPE_JSON = "application/json";
	private static final String URI = "/api/v1/mutant";
	@InjectMocks
	private ControladorMutante controlador;
	@Mock
	private MutantsServiceImpl mutantsService;
	private MockMvc mockMvc;

	private String[] dnaMutant = new String[] { "TTTTGA", "ATGTGC", "AGTTGG", "AGATGG", "CCCCTA", "TCGCTG" };

	private String[] dnaHuman = new String[] { "AAAAAA", "TCCTTC", "GTCTGG", "TGTTTG", "ACAGTA", "ACTCAG" };

	private String[] invalidDNA = new String[] { "ABGCGA", "CAGTGC", "TTATGG", "AGAAGG", "CCCCTA" };

	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(controlador).build();
	}

	@Test
	public void testAGivenDNAIsRecognizedAsMutant() throws Exception {

		String mockMutantBody = "{\"dna\":[\"TTTTGA\",\"ATGTGC\",\"AGTTGG\",\"AGATGG\",\"CCCCTA\",\"TCGCTG\"]}";

		mockIsMutantService(dnaMutant, true);

		ResultActions resultActions = mockMvc.perform(post(URI).contentType(CONTENT_TYPE_JSON).content(mockMutantBody));

		MvcResult result = resultActions.andExpect(status().isOk()).andReturn();
		Assert.isTrue(result.getResponse().getContentAsString().isEmpty(), "Response body must be empty");

	}

	@Test
	public void testAGivenDNAIsRecognizedAsHuman() throws Exception {

		String mockHumanBody = "{\"dna\":[\"AAAAAA\",\"TCCTTC\",\"GTCTGG\",\"TGTTTG\",\"ACAGTA\",\"ACTCAG\"]}";

		mockIsMutantService(dnaHuman, false);

		ResultActions resultActions = mockMvc.perform(post(URI).contentType(CONTENT_TYPE_JSON).content(mockHumanBody));

		MvcResult result = resultActions.andExpect(status().isForbidden()).andReturn();
		Assert.isTrue(result.getResponse().getContentAsString().isEmpty(), "Response body must be empty");
	}

	@Test
	public void testAnalizeMutantCandidateIsBadRequest() throws Exception {

		String mockAlienBody = "{\"dna\":[\"ABGCGA\",\"CAGTGC\",\"TTATGG\",\"AGAAGG\",\"CCCCTA\"]}";

		Mockito.doThrow(new InputValidationException("Alien DNA")).when(mutantsService).isMutant(invalidDNA);

		ResultActions resultActions = mockMvc.perform(post(URI).contentType(CONTENT_TYPE_JSON).content(mockAlienBody));

		MvcResult result = resultActions.andExpect(status().isBadRequest()).andReturn();
		Assert.isTrue(result.getResponse().getContentAsString().isEmpty(), "Response body must be empty");
	}

	private void mockIsMutantService(String[] dna, boolean expectedResult)	throws InputValidationException, ServiceException {
		Mockito.when(mutantsService.isMutant(dna)).thenReturn(expectedResult);
	}

}
