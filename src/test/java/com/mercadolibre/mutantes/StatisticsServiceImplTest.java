package com.mercadolibre.mutantes;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.mercadolibre.mutantes.dao.DAOService;
import com.mercadolibre.mutantes.exception.DbException;
import com.mercadolibre.mutantes.exception.ServiceException;
import com.mercadolibre.mutantes.service.StatisticsServiceImpl;

import junit.framework.TestCase;

/**
 * 2021/10/18
 * 
 * @author Maikol Arley Cucunuba Salazar
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class StatisticsServiceImplTest {


    @InjectMocks
    private StatisticsServiceImpl statisticsService;

    @Mock
    private DAOService daoService;

    @Test
    public void testStatsReturnStatsSuccessfully() throws DbException, ServiceException {

        // Mocks the content of the BD and return the quantity of Mutants
        Mockito.when(daoService.getMutantsCount()).thenReturn(1);
        // Mocks the content of the BD and return the quantity of Humans
        Mockito.when(daoService.getHumansCount()).thenReturn(10);

        // Return the stats Object
        TestCase.assertNotNull(statisticsService.getStats());
    }
}

