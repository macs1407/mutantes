package com.mercadolibre.mutantes;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Assert;

import com.mercadolibre.mutantes.controller.ControladorEstado;
import com.mercadolibre.mutantes.model.Statistics;
import com.mercadolibre.mutantes.service.StatisticsServiceImpl;

/**
 * 2021/10/18
 * @author Maikol Arley Cucunuba Salazar
 *
 */
public class ControladorEstadoTest {
	
	private static final String URI = "/api/v1/stats";
    @InjectMocks
    private ControladorEstado controller;
    @Mock
    private StatisticsServiceImpl statisticsService;
    private MockMvc mockMvc;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testStatisticsController() throws Exception {
        Mockito.when(statisticsService.getStats()).thenReturn(new Statistics(1, 10));
        ResultActions resultActions = mockMvc.perform(get(URI));
        MvcResult result = resultActions.andExpect(status().isOk()).andReturn();
        Assert.isTrue(!result.getResponse().getContentAsString().isEmpty(), "El response no debe estar vacio");
    }


}
