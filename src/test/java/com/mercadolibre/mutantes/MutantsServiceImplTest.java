package com.mercadolibre.mutantes;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.mercadolibre.mutantes.dao.DAOService;
import com.mercadolibre.mutantes.exception.DbException;
import com.mercadolibre.mutantes.exception.InputValidationException;
import com.mercadolibre.mutantes.exception.ServiceException;
import com.mercadolibre.mutantes.model.Subject;
import com.mercadolibre.mutantes.service.MutantsServiceImpl;

/**
 * 2021/10/18
 * 
 * @author Maikol Arley Cucunuba Salazar
 *
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class MutantsServiceImplTest {

    @InjectMocks
    private MutantsServiceImpl service;

    @Mock
    private DAOService daoService;

    private String[] dnaAlien1 = new String[]{"ABGCGA", "CAGTGC", "TTATGG", "AGAAGG", "CCCCTA", "TCGCTG"};

    private String[] dnaAlien2 = new String[]{"ABG", "CAG", "TTA", "AGA", "CCC", "TCG"};

    private String[] dnaMutant1 = new String[]{"TTTTGA", "ATGTGC", "AGTTGG", "AGATGG", "CCCCTA", "TCGCTG"};

    private String[] dnaMutant2 = new String[]{"GGGCGA", "CACAGC", "TCAAGG", "CGAAAG", "CCCATA", "TCCCCG"};

    private String[] dnaHuman1 = new String[]{"AAAAAA", "TCCTTC", "GTCTGG", "TGTTTG", "ACAGTA", "ACTCAG"};

    private String[] dnaHuman2 = new String[]{"AATGGA", "AATTGC", "TTGTGG", "CGGTAG", "CAAGTA", "TTTTTG"};

    private String[] getMutantDNA(String[] dna) throws DbException {

        Subject mutant = new Subject(dna, "mutant");
        Mockito.doNothing().when(daoService).insert(Mockito.eq(mutant));

        return dna;
    }

    private String[] getHumanDNA(String[] dna) throws DbException {

        Subject human = new Subject(dna, "human");
        Mockito.doNothing().when(daoService).insert(Mockito.eq(human));

        return dna;
    }

    @Test(expected = InputValidationException.class)
    public void testIsAlien1() throws InputValidationException, ServiceException {

        service.isMutant(dnaAlien1);
    }

    @Test(expected = InputValidationException.class)
    public void testIsAlien2() throws InputValidationException, ServiceException {

        service.isMutant(dnaAlien2);
    }

    @Test
    public void testIsMutant_Success_Mutant1() throws InputValidationException, ServiceException, DbException {

        boolean result = service.isMutant(getMutantDNA(dnaMutant1));
        TestCase.assertTrue(result);
    }

    @Test
    public void testIsMutant_Success_Mutant2() throws InputValidationException, ServiceException, DbException {

        boolean result = service.isMutant(getMutantDNA(dnaMutant2));
        TestCase.assertTrue(result);
    }

    @Test
    public void testIsMutant_Error_Human1() throws InputValidationException, ServiceException, DbException {

        boolean result = service.isMutant(getHumanDNA(dnaHuman1));
        TestCase.assertFalse(result);
    }

    @Test
    public void testIsMutant_Error_Human2() throws InputValidationException, ServiceException, DbException {

        boolean result = service.isMutant(getHumanDNA(dnaHuman2));
        TestCase.assertFalse(result);
    }


}
