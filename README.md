# Mercado Libre, Maikol Arley Cucunuba Salazar, 2021/10/18

Examen mutantes para MercadoLibre. 

Introducción:
El desarollo que se ha realizado tiene como proposito 2 funcionalidades:
 - Verificar el ADN para determinar si corresponde a un Humano o Mutante.
 - Consultar el ratio de mutantes/humanos luego de cada analisis que fue realizado con cada api.
 
# Indice

-  [Implementación y tecnologias usadas](#implementaci%C3%B3n-y-tecnologias-usadas)
-  [Instrucciones](#instrucciones)
-  [Uso](#uso)
-  [API Url](#api)
-  [Servicios](#servicios)
-  [Es mutante](#es-mutante)
-  [Estadisticas](#estadisticas)
-  [Test](#test)
-  [Conclusiones](#conclusiones)
  
### Implementacion y tecnologias usadas

- [Spring Boot](https://projects.spring.io/spring-boot/)
- [SL4FJ](https://www.slf4j.org/)
- [MongoDb](https://www.mongodb.com/)
- [Mongo Atlas](https://www.mongodb.com/es)

### Instrucciones
```
mvn exec:java -Dexec.mainClass="com.mercadolibre.mutantes.Application"
```

### Uso

Pasos para desplegar de forma local:
1-) descargar el codigo fuente en un comprimido o haciendo un clon del proyecto
2-) con su IDE preferido importar el proyecto con tecnologia maven
3-) correr el proyecto, por defecto este en el puerto 8090
4-) para consumir las apis utilizar su cliente peferido en este caso utilice Postman

### API Url

URL local: http://localhost:8080

### Servicios
#### Es mutante

Request: 
- POST http://localhost:8090/api/v1/mutant

Request body (secuencia ADN mutante):

```
  {"dna":["TTTTGA", "ATGTGC", "AGTTGG", "AGATGG", "CCCCTA", "TCGCTG"]}
```

Response:

```
  200 OK
```
Request body (secuencia ADN humano):

```
  {"dna":["AAAAAA", "TCCTTC", "GTCTGG", "TGTTTG", "ACAGTA", "ACTCAG"]}
```

Response:

```
  403 Forbidden
```
Request body (secuencia ADN erronea):

```
  {"dna":["ABGCGA", "CAGTGC", "TTATGG", "AGAAGG", "CCCCTA", "TCGCTG"]}
```

Response:

```
  400 Bad RequestForbidden
```

Request body (La secuancia ya existe en base de datos):

```
  {"dna":["TTTTGA", "ATGTGC", "AGTTGG", "AGATGG", "CCCCTA", "TCGCTG"]}
  500 Internal Server Error

```

#### Estadisticas

Request: 
- GET http://localhost:8090/api/v1/stats

Response: 200 (application/json)

```
{
    count_mutant_dna: 3,
    count_human_dna: 2,
    ratio: 1.5
}
```

#### Automaticos

Para las pruebas unitarias se utilizo spring-boot-starter-test y mockito, se puede ver la cobertura en la carpeta raiz que tiene nombre reporte cobertura
abrir el archivo index.html

#### Conclusiones

Trate de realizarlo lo mejor posible integrando varias tecnologias y cumpliendo con los puntos que estaban en el reto, no me fue posible subir a AWS ya que
cuando iba a generar la imagen docker del proyecto me esta arrojando error